Ein Projekt von Tobias Behn, Jonas Christmann, Johanna Meyer und Bennedict Schweimer.
Entstandne im Modul "Mobile Systeme" an der HAW-Hamburg.

## User Stories

I. Als User möchte ich eine Augmented Reality App nutzen, um mich mithilfe 
des Handys im Raum zu bewegen und mich an themenrelevante Orte zu 
begeben und mich dort umzusehen.

II. Als User möchte ich mich über Portale aus dem Ausstellungsraum in eine 
themenrelevante Szenerie bewegen, um dort die Probleme der heutigen 
Unterwasserwelt hautnah zu erleben.

III. Als User möchte ich die Szenerie über ein 360° Video wahrnehmen, um ein 
realistisches Abbild der Situation zu bekommen.

IV. Als User möchte ich verschiedene Unterwasserwelten besuchen können, um 
mir über das Ausmaß des Problems klar zu werden.

V. Als User möchte ich Informationen zu den Szenerien angeboten bekommen, 
um mich weiter mit den Problemen auseinandersetzen zu können.
VI. Als User möchte ich diese Informationen auf Wunsch ausblenden können, 
um das Erlebnis der AR-App nicht einzuschränken.

VII. Als User möchte ich in der AR-App mit Müll (Plastiktüten, Plastikflaschen, 
Plastikbesteck, Getränkedosen, Strohhalme) konfrontiert werden, um die 
Verschmutzung direkt zu erleben.

VIII. Als User möchte ich den Müll per Touch einsammeln können, um 
symbolisch das Meer zu reinigen.

IX. Als User möchte ich die App über einen Button schließen können, um diese 
zu verlassen.

X. Als User möchte ich sehen, wie viel Müll insgesamt gesammelt wurde und 
dies mit Statistiken vergleichen, um einschätzen zu können, wie viel/wenig 
die Bemühungen bewirkt haben.

## Use-Case-Diagramm

![Use-Case-Diagramm](Images/UseCaseUWP.png)

## Screenshots

![Bild1](Images/UWP1.png) ![Bild2](Images/UWP2.png)

![Bild3](Images/UWP3.png) ![Bild4](Images/UWP4.png)
